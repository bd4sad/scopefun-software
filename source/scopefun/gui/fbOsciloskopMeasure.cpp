#include <scopefun/gui/fbOsciloskopMeasure.h>

OsciloskopMeasure::OsciloskopMeasure( wxWindow* parent )
:
Measure( parent )
{

}

void OsciloskopMeasure::DisplayOnInitDialog( wxInitDialogEvent& event )
{
    // TODO: Implement DisplayOnInitDialog
}

void OsciloskopMeasure::m_checkBoxAutoClearOnCheckBox( wxCommandEvent& event )
{
    // TODO: Implement m_checkBoxAutoClearOnCheckBox
}

void OsciloskopMeasure::m_checkBoxAutoClearTriggerOnCheckBox( wxCommandEvent& event )
{
    // TODO: Implement m_checkBoxAutoClearTriggerOnCheckBox
}

void OsciloskopMeasure::m_buttonHistoryClearOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonHistoryClearOnButtonClick
}

void OsciloskopMeasure::m_buttonClearXOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonClearXOnButtonClick
}

void OsciloskopMeasure::m_buttonClearYOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonClearYOnButtonClick
}

void OsciloskopMeasure::m_buttonClearFFTOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonClearFFTOnButtonClick
}

void OsciloskopMeasure::m_buttonCopyOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonCopyOnButtonClick
}

void OsciloskopMeasure::m_buttonPickX0OnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonPickX0OnButtonClick
}

void OsciloskopMeasure::m_textCtrlX0OnTextEnter( wxCommandEvent& event )
{
    // TODO: Implement m_textCtrlX0OnTextEnter
}

void OsciloskopMeasure::m_spinBtnX0OnSpinDown( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnX0OnSpinDown
}

void OsciloskopMeasure::m_spinBtnX0OnSpinUp( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnX0OnSpinUp
}

void OsciloskopMeasure::m_sliderX0OnScroll( wxScrollEvent& event )
{
    // TODO: Implement m_sliderX0OnScroll
}

void OsciloskopMeasure::m_buttonPickX1OnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonPickX1OnButtonClick
}

void OsciloskopMeasure::m_textCtrlX1OnTextEnter( wxCommandEvent& event )
{
    // TODO: Implement m_textCtrlX1OnTextEnter
}

void OsciloskopMeasure::m_spinBtnX1OnSpinDown( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnX1OnSpinDown
}

void OsciloskopMeasure::m_spinBtnX1OnSpinUp( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnX1OnSpinUp
}

void OsciloskopMeasure::m_sliderX1OnScroll( wxScrollEvent& event )
{
    // TODO: Implement m_sliderX1OnScroll
}

void OsciloskopMeasure::m_buttonPickY0OnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonPickY0OnButtonClick
}

void OsciloskopMeasure::m_textCtrlY0OnTextEnter( wxCommandEvent& event )
{
    // TODO: Implement m_textCtrlY0OnTextEnter
}

void OsciloskopMeasure::m_choiceY0OnChoice( wxCommandEvent& event )
{
    // TODO: Implement m_choiceY0OnChoice
}

void OsciloskopMeasure::m_spinBtnY0OnSpinDown( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnY0OnSpinDown
}

void OsciloskopMeasure::m_spinBtnY0OnSpinUp( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnY0OnSpinUp
}

void OsciloskopMeasure::m_sliderY0OnScroll( wxScrollEvent& event )
{
    // TODO: Implement m_sliderY0OnScroll
}

void OsciloskopMeasure::m_buttonPickY1OnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonPickY1OnButtonClick
}

void OsciloskopMeasure::m_textCtrlY1OnTextEnter( wxCommandEvent& event )
{
    // TODO: Implement m_textCtrlY1OnTextEnter
}

void OsciloskopMeasure::m_choiceY1OnChoice( wxCommandEvent& event )
{
    // TODO: Implement m_choiceY1OnChoice
}

void OsciloskopMeasure::m_spinBtnY1OnSpinDown( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnY1OnSpinDown
}

void OsciloskopMeasure::m_spinBtnY1OnSpinUp( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnY1OnSpinUp
}

void OsciloskopMeasure::m_sliderY1OnScroll( wxScrollEvent& event )
{
    // TODO: Implement m_sliderY1OnScroll
}

void OsciloskopMeasure::m_buttonPickFFT0OnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonPickFFT0OnButtonClick
}

void OsciloskopMeasure::m_textCtrlFFT0OnTextEnter( wxCommandEvent& event )
{
    // TODO: Implement m_textCtrlFFT0OnTextEnter
}

void OsciloskopMeasure::m_spinBtnFFT0OnSpinDown( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnFFT0OnSpinDown
}

void OsciloskopMeasure::m_spinBtnFFT0OnSpinUp( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnFFT0OnSpinUp
}

void OsciloskopMeasure::m_sliderFFT0OnScroll( wxScrollEvent& event )
{
    // TODO: Implement m_sliderFFT0OnScroll
}

void OsciloskopMeasure::m_buttonPickFFT1OnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonPickFFT1OnButtonClick
}

void OsciloskopMeasure::m_textCtrlFFT1OnTextEnter( wxCommandEvent& event )
{
    // TODO: Implement m_textCtrlFFT1OnTextEnter
}

void OsciloskopMeasure::m_spinBtnFFT1OnSpinDown( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnFFT1OnSpinDown
}

void OsciloskopMeasure::m_spinBtnFFT1OnSpinUp( wxSpinEvent& event )
{
    // TODO: Implement m_spinBtnFFT1OnSpinUp
}

void OsciloskopMeasure::m_sliderFFT1OnScroll( wxScrollEvent& event )
{
    // TODO: Implement m_sliderFFT1OnScroll
}

void OsciloskopMeasure::m_ItemActivated( wxDataViewEvent& event )
{
// TODO: Implement m_ItemActivated
}

void OsciloskopMeasure::m_ItemStartEditing( wxDataViewEvent& event )
{
// TODO: Implement m_ItemStartEditing
}

void OsciloskopMeasure::m_SelectionChanged1( wxDataViewEvent& event )
{
    // TODO: Implement m_SelectionChanged1
}

void OsciloskopMeasure::m_OnLeftDown( wxMouseEvent& event )
{
    // TODO: Implement m_OnLeftDown
}

void OsciloskopMeasure::m_dataViewListCtrl1OnMouseEvents( wxMouseEvent& event )
{
    // TODO: Implement m_dataViewListCtrl1OnMouseEvents
}

void OsciloskopMeasure::m_buttonOkOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonOkOnButtonClick
}

void OsciloskopMeasure::m_buttonDefaultOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonDefaultOnButtonClick
}

void OsciloskopMeasure::m_buttonCancelOnButtonClick( wxCommandEvent& event )
{
    // TODO: Implement m_buttonCancelOnButtonClick
}
